#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

#define MAX_SLEEP_TIME 500000000

int threads_count = 0;
int threads_in_cs = 0;
int ticket_id = 0;
int active_ticket = 0;
pthread_mutex_t mutex_ticket = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_await = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
struct thread_info {
    pthread_t thread_id;
    int thread_order_num;
};

/**
 * @brief Prints help message
 */
void printHelp(void) {
    printf(
        "+----------------------------+\n"
        "| Project: Ticket Algorithm  |\n"
        "|  Author: Daniel Haris      |\n"
        "|   Login: xharis00          |\n"
        "+----------------------------+\n"
        "| Usage:                     |\n"
        "| ./proj1 N M                |\n"
        "| N - num of created threads |\n"
        "| M - num of threads in CS   |\n"
        "+----------------------------+\n"
    );
}

/**
 * @brief Generates unique ticket numbers starting from 0
 * @return Unique ticket number
 */
int getticket(void) {
    pthread_mutex_lock(&mutex_ticket);
    int actual_ticket = ticket_id++;
    pthread_mutex_unlock(&mutex_ticket);
    return actual_ticket;
}

/**
 * @brief critical section entry
 * @param aenter ticket number
 */
void await(int aenter) {
    pthread_mutex_lock(&mutex_await);
    while (aenter != active_ticket)
        pthread_cond_wait(&cond, &mutex_await);
}

/**
 * @brief critical section exit
 */
void advance(void) {
    ++active_ticket;
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mutex_await);
}

/**
 * @brief random waiting from 0s to 0.5s
 * @param threadNum custom thread ID
 */
void randomWait(int threadNum) {
    unsigned int seed = threadNum + time(NULL);
    struct timespec r;
    r.tv_sec = 0;
    r.tv_nsec = rand_r(&seed) % MAX_SLEEP_TIME;
    nanosleep(&r, NULL);
}

/**
 * @brief function run in parallel - ticket algorithm
 * @param t thread routine arguments
 */
void *threadRoutine(void *t) {
    int ticket;
    struct thread_info *tc = t;
    // ticket assign
    while ((ticket = getticket()) < threads_in_cs) {
        // random wait in interval <0.0s, 0.5s>
        randomWait((int) tc->thread_order_num);
        // CS entry
        await(ticket);
        printf("%d\t(%d)\n", ticket, (int) tc->thread_order_num);
        // CS exit
        advance();
        // random wait in interval <0.0s, 0.5s>
        randomWait((int) tc->thread_order_num);
    }
    pthread_exit(NULL);
}

/**
 * @brief main function
 * @param argc arguments count
 * @param argv arguments in string
 * @return integer
 */
int main(int argc, char** argv) {

    char *ptr1, *ptr2;
    if (argc != 3) {
        printHelp();
        fprintf(stderr, "\nInvalid arguments inserted.\n");
        return -1;
    }

    threads_count = strtol(argv[1], &ptr1, 10);
    threads_in_cs = strtol(argv[2], &ptr2, 10);
    if (*ptr1 != '\0' || *ptr2 != '\0' || threads_count < 1 || threads_in_cs < 1) {
        printHelp();
        fprintf(stderr, "\nParameters N and M must be positive integers.\n");
        return -1;
    }

    struct thread_info *threads;

    if (!(threads = calloc((size_t) threads_count, sizeof(struct thread_info)))) {
        fprintf(stderr, "Error allocating threads info structure.\n");
        return -2;
    }

    for (int i = 0; i < threads_count; i++) {
        threads[i].thread_order_num = i + 1;
        if (pthread_create(&threads[i].thread_id, NULL, threadRoutine, &threads[i])) {
            fprintf(stderr, "Error creating threads.\n");
            return -2;
        }
    }

    for (int i = 0; i < threads_count; i++) {
        if (pthread_join(threads[i].thread_id, NULL)) {
            fprintf(stderr, "Error joining threads.\n");
            return -2;
        }
    }

    free(threads);
    pthread_mutex_destroy(&mutex_ticket);
    pthread_mutex_destroy(&mutex_await);
    pthread_cond_destroy(&cond);

    return 0;
}
