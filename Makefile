CC=gcc
CFLAGS=-std=gnu99 -pedantic -Wextra -Wall -Werror -g -pthread
PROJ=proj1

all:
	$(CC) $(CFLAGS) $(PROJ).c -o $(PROJ)
clean:
	rm $(PROJ)

